<?php

function redirectTo($url,$exit=TRUE)
{
    if(!headers_sent())
    {
        header("Location:" . $url);
    }  else {
        echo '<script language="javascript">window.location.href="'.addslashes($url).'";</script>';
    }
    
    if($exit === TRUE)
        exit;
}

function getFunctionArgs()
{
    global $soapClient,$soapArgs;
    if($soapClient instanceof  SoapClient)
    {
        $soapArgs["help"] = true;

        $res = $soapClient->__call('genericExecute', array(new SoapParam($soapArgs,'param')));

        unset($res["help"],$soapArgs["help"]);

        return $res;
    }  else {
        return false;
    }
}