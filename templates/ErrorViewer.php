<div class="errors">
    <?php
        if(is_array($errors) && count($errors) > 0)
        {
            if(count($errors) == 1)
            {
                ?>
    <p><?php echo $errors[0]; ?></p>
                <?php
            }  else {
                ?>
    <ul>
        <?php
                        foreach ($errors as $error)
                        {
                            ?>
        <li><?php echo $error; ?></li>
                            <?php
                        }
        ?>
    </ul>
                <?php
            }
        }
    ?>
</div>
