<div id="methodForm">
    <h4>Formulaire de la méthode "<em><?php echo $_GET["method"]; ?></em>"</h4>
    <form method="post" action="">
        <?php
            foreach($methodFunctions as $key => $value)
            {
        ?>
        <p>
            <label for="<?php echo $key; ?>"><?php echo $key; ?></label>
            <input type="text" name="<?php echo $key; ?>" id="<?php echo $key; ?>" />
            <span><?php echo $value; ?></span>
        </p>
        <?php
            }
        ?>
        <input type="submit" value="Exécuter" />
    </form>
</div>