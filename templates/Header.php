<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bienvenue <?php echo $_SESSION["glpi.infos"]["realname"]." " . $_SESSION["glpi.infos"]["firstname"] ."(".$_SESSION["glpi.infos"]["name"].")"; ?></title>
        <style type="text/css">
            div#main
            {
                width: 800px;
                margin: 0 auto;
            }
            div#mainMenu
            {
                width: 300px;
            }
            #methodForm
            {
                width: 600px;
                margin: 0 auto;
                margin-top: 30px;

            }
            #methodForm form p
            {
                margin-top: 10px;
                overflow: auto;
            }

            #methodForm form span
            {
                display: block;
                float: left;
                width: 180px;
                padding-left: 20px;
            }

            #methodForm label
            {
                display: block;
                width:150px;
                float:left;
                
            }
            #methodForm input
            {
                display: block;
                width:120px;
                float:right;
            }
            #methodForm form p input
            {
                display: block;
                width:250px;
                float:left;
            }
            .errors
            {
                color: red;
            }
        </style>
    </head>
    <body>
        <div id="main">
            <div id="mainMenu">
                <ul>
                    <li><a href="index.php">Liste des méthodes</a></li>
                    <li><a href="index.php?action=Logout">Déconnexion (<?php echo $_SESSION["glpi.infos"]["realname"]." " . $_SESSION["glpi.infos"]["firstname"]; ?>)</a></li>
                </ul>
            </div>