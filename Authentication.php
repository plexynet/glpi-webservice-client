<?php

if(isset($_SESSION["glpi.authenticated"]) && $_SESSION["glpi.authenticated"] = TRUE)
{
    redirectTo("index.php");
} else {
    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $errors = array();
        $username = $_POST["Username"];
        $password = $_POST["Password"];

        if($username == NULL || $username=="")
            $errors[] = "Le nom d'utilisateur est obligatoire";
		
        elseif(strlen($username) < 3 || strlen($username) > 15)
            $errors[] = "Le nom d'utilisateur doit être entre 3 et 15 caractères de longueur.";
		
		
        if($password == NULL || $password=="")
            $errors[] = "Le Mot de passe est obligatoire";
        elseif(strlen($password) < 3 || strlen($password) > 15)
            $errors[] = "Le mot de passe doit être entre 3 et 15 caractères de longueur.";
		
		
        if(count($errors) == 0)
        {
            $args = array("login_name"=>$username,"login_password"=>$password,"method"=>"glpi.doLogin");
            try
            {
                $result = $soapClient->__call('genericExecute', array(new SoapParam($args,'param')));
                $_SESSION["glpi.infos"] = $result;
                $_SESSION["glpi.authenticated"] = TRUE;
                redirectTo("index.php");
            }catch (SoapFault $fault)
            {
                $errors[] = $fault->faultcode . " : " . $fault->faultstring;
            }

        }
    }

    include "templates/Authenticate.php";
}
?>