<?php

if(!extension_loaded("soap"))
    die("L'extension SOAP n'est pas chargée.\n");

@session_start();
require_once 'conf.inc.php';
require_once 'functions.inc.php';

$soapClient = new SoapClient(null,array('uri'=>$soapUrl,'location'=>$soapUrl));

if(!isset($_SESSION["glpi.authenticated"]) || $_SESSION["glpi.authenticated"] != TRUE)
{

    include 'Authentication.php';

}else
{

    $soapArgs = array("session" => $_SESSION["glpi.infos"]["session"]);
    include "templates/Header.php";

    if(isset($_GET["action"]))
    {
        if($_GET["action"] == "execMethod" && (isset($_GET["method"]) && is_string($_GET["method"])))
        {

            include 'MethodExecutor.php';
        }elseif($_GET["action"] == "Logout")
        {

            include 'Logout.php';
        }
    }else
    {

        include 'MethodRetriever.php';
    }

    include "templates/Footer.php";

}

?>